#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rospy
import torch

import numpy as np
import struct
import queue
from threading import Thread
from std_msgs.msg import String
from hri_msgs.msg import IdsList, LiveSpeech, LiveSpeechId

import queue
import sys
import grpc
import riva_api.riva_asr_pb2 as rasr
import riva_api.riva_asr_pb2_grpc as rasr_srv
import riva_api.riva_audio_pb2 as ra
from router_node import router
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus

TIMEOUT_SEC = 2
INDEX = sys.argv[-2][-1]
RATE = 16000
CHUNK=512
VERBOSE = rospy.get_param(f'/riva_asr_{INDEX}/verbose')
LANG = rospy.get_param(f"/riva_asr_{INDEX}/lang")
SERVER = rospy.get_param(f'/riva_asr_{INDEX}/server')
DISABLE_ID = rospy.get_param(f'/riva_asr_{INDEX}/disable_id')
if SERVER.endswith('50051'):
    pass
else: SERVER += ':50051'
use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")
if rospy.get_param(f"riva_asr_{INDEX}/topic") == "/audio/enh_audio":
    from spring_msgs.msg import RawAudioData
else: from respeaker_ros.msg import RawAudioData 


class AudioProcessing(object):
    def __init__(self, name):
        self._buff = queue.Queue() # Create a thread-safe buffer of audio data
        self.chunk = CHUNK
        rospy.loginfo("Starting {}.".format(name))
        rospy.loginfo("RIVA server ip:{}".format(SERVER))
        rospy.loginfo(f'listening to topic:{rospy.get_param(f"/riva_asr_{INDEX}/topic")} ')
        if VERBOSE:
            rospy.loginfo("verbose")
        self.last_text_time = rospy.Time.now()
        self.__is_alive()
        self.closed = False
        self.router = router(INDEX,DISABLE_ID)
        self.currnet_speech_pub_inc =rospy.Publisher(f'/humans/voices/current_speaker_{INDEX}/speech/incremental',LiveSpeechId,queue_size=1)
        self.currnet_speech_pub_final =rospy.Publisher(f'/humans/voices/current_speaker_{INDEX}/speech/final',LiveSpeechId,queue_size=1)
       
    def __enter__(self):
        rospy.Subscriber(rospy.get_param(f"/riva_asr_{INDEX}/topic"), RawAudioData, self.callback, queue_size=10)
        return self

    def __exit__(self, type, value, traceback):
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        sys.exit(0)

    def __is_alive(self):
        TH = rospy.Duration(15)
        pub = rospy.Publisher("~is_alive", String, queue_size=1)
        diagnostics_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=1)
        diagnostocs_arr = DiagnosticArray()
        def publish():
            r = rospy.Rate(1)
            while not rospy.is_shutdown():
                if (rospy.Time.now() - self.last_text_time) > TH:
                    msg1 = DiagnosticStatus(level=DiagnosticStatus.WARN,name='Audio: Audio preprocessing: ASR',message=f'No ASR transcription for more then {float(TH.secs +TH.nsecs*(10**-9))} secs')
                else:
                    msg1 = DiagnosticStatus(level=DiagnosticStatus.OK,name='Audio: Audio preprocessing: ASR',message='OK')
                diagnostocs_arr.status = [msg1]
                diagnostocs_arr.header.stamp=rospy.Time.now()
                diagnostics_pub.publish(diagnostocs_arr)
                pub.publish(str(rospy.Time.now().to_sec()))
                r.sleep()
        t = Thread(target=publish)
        t.start()
     

    def callback(self, msg):
        if rospy.get_param(f"riva_asr_{INDEX}/topic") == "/audio/raw_audio":
            d = np.array(msg.data).reshape(512,6)[:,1]
            frame = [struct.pack('h',sample) for sample in d]
        else:
            frame = [struct.pack('h',sample) for sample in msg.data]
        data = b''.join(frame)
        self._buff.put(data)
        if self.closed:
            sys.exit(0)
        return None

    def generator(self):
        while not self.closed:
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)

    def listen_print_loop(self, responses):
        num_chars_printed = 0
        for response in responses:
            if VERBOSE:
                rospy.loginfo(response)
            if not response.results:
                continue

            partial_transcript = ""
            for result in response.results:
                
                if not result.alternatives:
                    continue

                transcript = result.alternatives[0].transcript
                self.last_text_time= rospy.Time.now()
                if not result.is_final:
                    partial_transcript += transcript
                else:
                    overwrite_chars = ' ' * (num_chars_printed - len(transcript))
                    msg_id =LiveSpeechId(final=transcript + overwrite_chars,id=self.router.current_speaker,confidence=1.0)
                    msg = LiveSpeech(final=transcript + overwrite_chars,confidence=1.0)
                    if VERBOSE: rospy.loginfo(self.router.current_speaker if self.router.current_speaker != '' else 'empty')
                    if self.router.current_speaker and self.router.current_speaker != '_':
                        if VERBOSE:
                            rospy.loginfo(f"publishing to /humans/voices/{self.router.current_speaker}/speech")
                        rospy.Publisher(f'/humans/voices/{self.router.current_speaker}/speech',LiveSpeech,queue_size=1).publish(msg)
                        self.currnet_speech_pub_final.publish(msg_id)
                        if VERBOSE:
                            rospy.loginfo("## " + transcript + overwrite_chars + "\n")
                        # self._buff.queue.clear()
                    num_chars_printed = 0

            if partial_transcript != "":
                overwrite_chars = ' ' * (num_chars_printed - len(partial_transcript))
                msg_id =LiveSpeechId(incremental=partial_transcript + overwrite_chars,id=self.router.current_speaker,confidence=1.0)
                msg = LiveSpeech(incremental=partial_transcript + overwrite_chars,confidence=1.0)
                if self.router.current_speaker:
                    rospy.Publisher(f'/humans/voices/{self.router.current_speaker}/speech',LiveSpeech,queue_size=1).publish(msg)
                    self.currnet_speech_pub_inc.publish(msg_id)
                    rospy.loginfo(msg)
                num_chars_printed = len(partial_transcript) + 3
         
def grpc_server_on(channel) -> bool:
    try:
        grpc.channel_ready_future(channel).result(timeout=TIMEOUT_SEC)
        return True
    except grpc.FutureTimeoutError:
        return False
    
def main():
    rospy.init_node("audio_processing")
    channel = grpc.insecure_channel(SERVER)
    status = grpc_server_on(channel)
    while not status:
        status = grpc_server_on(channel)
    client = rasr_srv.RivaSpeechRecognitionStub(channel)

    config = rasr.RecognitionConfig(
        encoding=ra.AudioEncoding.LINEAR_PCM,
        sample_rate_hertz=RATE,
        language_code=LANG,
        max_alternatives=1,
        enable_automatic_punctuation=True,
    )
    streaming_config = rasr.StreamingRecognitionConfig(config=config, interim_results=True)

    with AudioProcessing('ASR') as stream:
        audio_generator = stream.generator()
        requests = (rasr.StreamingRecognizeRequest(audio_content=content) for content in audio_generator)

        def build_generator(cfg, gen):
            yield rasr.StreamingRecognizeRequest(streaming_config=cfg)
            for x in gen:
                yield x
        
        responses = client.StreamingRecognize(build_generator(streaming_config, requests))
        
        stream.listen_print_loop(responses)

if __name__ == "__main__":
    main()

