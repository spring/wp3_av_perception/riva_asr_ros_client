import rospy
import torch
from respeaker_ros.msg import RawAudioData
from hri_msgs.msg import IdsList

class router():
    def __init__(self,index,disable=False):
        if disable:
            self.index = index
            self.current_speaker = f'voice_{index}'
        else:
            self.index = index
            self.current_speaker = ''
            self.sub(index)
    def sub(self,index):
            rospy.Subscriber(f"/humans/voices/tracked", IdsList, self.callback)
    def callback(self,msg):
        self.current_speaker = str(msg.ids[int(self.index)])
        