RIVA ASR client for ARI.

This package will start an asr client using NVIDIA-RIVA.

to launch the package use:

```
roslaunch riva_asr riva_asr_launch.launch audio_topic:=/audio/enh_audio riva_server:=132.70.226.188 verbose:=True mock_id:=True
```
audio_topic: topic to listen (enh/raw)

riva_server: RIVA server ip (default is localhost)

verbose: print the output to terminal (default is False)

mock_id: if not used with speaker_id set mock to true to activate the ASR 

